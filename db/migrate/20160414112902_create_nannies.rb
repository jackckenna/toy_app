class CreateNannies < ActiveRecord::Migration
  def change
    create_table :nannies do |t|
      t.string :name
      t.string :email
      t.string :phone_no
      t.text :welcome_message
      t.timestamp :date_time

      t.timestamps null: false
    end
  end
end
