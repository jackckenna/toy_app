require 'test_helper'

class NanniesControllerTest < ActionController::TestCase
  setup do
    @nanny = nannies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:nannies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create nanny" do
    assert_difference('Nanny.count') do
      post :create, nanny: { date_time: @nanny.date_time, email: @nanny.email, name: @nanny.name, phone_no: @nanny.phone_no, welcome_message: @nanny.welcome_message }
    end

    assert_redirected_to nanny_path(assigns(:nanny))
  end

  test "should show nanny" do
    get :show, id: @nanny
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @nanny
    assert_response :success
  end

  test "should update nanny" do
    patch :update, id: @nanny, nanny: { date_time: @nanny.date_time, email: @nanny.email, name: @nanny.name, phone_no: @nanny.phone_no, welcome_message: @nanny.welcome_message }
    assert_redirected_to nanny_path(assigns(:nanny))
  end

  test "should destroy nanny" do
    assert_difference('Nanny.count', -1) do
      delete :destroy, id: @nanny
    end

    assert_redirected_to nannies_path
  end
end
