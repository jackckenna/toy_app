json.array!(@nannies) do |nanny|
  json.extract! nanny, :id, :name, :email, :phone_no, :welcome_message, :date_time
  json.url nanny_url(nanny, format: :json)
end
