class Nanny < ActiveRecord::Base
  validates :name, presence: true
  validates :email, presence: true
  validates :phone_no, presence: true
  validates :welcome_message, length: { maximum: 200 },
  presence: true
end
